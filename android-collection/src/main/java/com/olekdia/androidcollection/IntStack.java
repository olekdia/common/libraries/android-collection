package com.olekdia.androidcollection;

import java.util.Arrays;

import static com.olekdia.androidcommon.ConstantsKt.INVALID;

/**
 * Frozen to release
 */
public class IntStack {

    /**
     * Default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * Default initial capacity increment
     */
    private static final int DEFAULT_CAP_INCREMENT = 5;

    protected int[] mArray;

    /**
     * The number of elements or the size of the vector.
     */
    protected int mSize;
    /**
     * How many elements should be added to the vector when it is detected that
     * it needs to grow to accommodate extra entries. If this value is zero or
     * negative the size will be doubled if an increase is needed.
     */
    protected int mCapacityInc;

    public IntStack(final int initialCapacity, final int initialCapIncrement) {
        if (initialCapacity > 0) {
            mArray = new int[initialCapacity];
        } else if (initialCapacity == 0) {
            mArray = ArrayHelper.EMPTY_INTS;
        }
        mCapacityInc = Math.min(initialCapIncrement, 0);
    }

    public IntStack(int initialCapacity) {
        this(initialCapacity, DEFAULT_CAP_INCREMENT);
    }

    public IntStack() {
        this(DEFAULT_CAPACITY, DEFAULT_CAP_INCREMENT);
    }

    public final boolean isEmpty() {
        return mSize == 0;
    }

    public final int size() {
        return mSize;
    }

    public final int push(final int value) {
        if (mSize == mArray.length) {
            grow();
        }
        mArray[mSize++] = value;
        return value;
    }

    public final void pushDistinct(final int value) {
        if (!contains(value)) push(value);
    }

    public final int pop() {
        if (mSize == 0) {
            return INVALID;
        } else {
            final int index = --mSize;
            final int value = mArray[index];
            mArray[index] = INVALID;
            return value;
        }
    }

    public final int get(final int index) {
        return mArray[index];
    }

    public final int indexOf(final int value) {
        final int[] arr = mArray;
        final int size = mSize;
        for (int i = 0; i < size; i++) {
            if (value == arr[i]) return i;
        }
        return -1;
    }

    public final boolean contains(final int value) {
        return indexOf(value) >= 0;
    }

    public final void clear() {
        if (mSize != 0) {
            Arrays.fill(mArray, 0, mSize, INVALID);
            mSize = 0;
        }
    }

    public final boolean remove(final int value) {
        final int index = indexOf(value);
        if (index == INVALID) {
            return false;
        } else {
            final int numMoved = mSize - index - 1;
            if (numMoved > 0) System.arraycopy(mArray, index + 1, mArray, index, numMoved);
            mSize--;
            return true;
        }
    }

    public final void setCapacityIncrement(final int value) {
        mCapacityInc = value;
    }

    private void grow() {
        grow(mCapacityInc <= 0 ? DEFAULT_CAP_INCREMENT : mCapacityInc);
    }

    private void grow(final int additionalSize) {
        final int[] newArray = new int[mArray.length + additionalSize];
        System.arraycopy(mArray, 0, newArray, 0, mSize);
        mArray = newArray;
    }
}