/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.androidcollection;

import static com.olekdia.androidcommon.ConstantsKt.INVALID;

public class LongKeySparseBooleanArray implements Cloneable {

    private long[] mKeys;
    private boolean[] mValues;
    private int mSize;

    /**
     * Creates a new SparseBooleanArray containing no mappings.
     */
    public LongKeySparseBooleanArray() {
        this(10);
    }

    /**
     * Creates a new SparseBooleanArray containing no mappings that will not
     * require any additional memory allocation to store the specified
     * number of mappings.  If you supply an initial capacity of 0, the
     * sparse array will be initialized with a light-weight representation
     * not requiring any additional array allocations.
     */
    public LongKeySparseBooleanArray(int initialCapacity) {
        if (initialCapacity == 0) {
            mKeys = ArrayHelper.EMPTY_LONGS;
            mValues = ArrayHelper.EMPTY_BOOLS;
        } else {
            initialCapacity = ArrayHelper.idealIntArraySize(initialCapacity);
            mKeys = new long[initialCapacity];
            mValues = new boolean[mKeys.length];
        }
        mSize = 0;
    }

    @Override
    public final LongKeySparseBooleanArray clone() {
        LongKeySparseBooleanArray clone = null;
        try {
            clone = (LongKeySparseBooleanArray) super.clone();
            clone.mKeys = mKeys.clone();
            clone.mValues = mValues.clone();
        } catch (CloneNotSupportedException cnse) {
            /* ignore */
        }
        return clone;
    }

    /**
     * Gets the boolean mapped from the specified key, or <code>false</code>
     * if no such mapping has been made.
     */
    public final boolean get(final long key) {
        return get(key, false);
    }

    /**
     * Gets the boolean mapped from the specified key, or the specified value
     * if no such mapping has been made.
     */
    public final boolean get(final long key, final boolean valueIfKeyNotFound) {
        final int i = ArrayHelper.binarySearch(mKeys, mSize, key);

        if (i < 0) {
            return valueIfKeyNotFound;
        } else {
            return mValues[i];
        }
    }

    /**
     * Removes the mapping from the specified key, if there was any.
     */
    public final boolean delete(final int key) {
        final int i = ArrayHelper.binarySearch(mKeys, mSize, key);

        if (i >= 0) {
            System.arraycopy(mKeys, i + 1, mKeys, i, mSize - (i + 1));
            System.arraycopy(mValues, i + 1, mValues, i, mSize - (i + 1));
            mSize--;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes the mapping at the specified index.
     * <p>
     * For indices outside of the range {@code 0...size()-1}, the behavior is undefined.
     */
    public final void removeAt(final int index) {
        System.arraycopy(mKeys, index + 1, mKeys, index, mSize - (index + 1));
        System.arraycopy(mValues, index + 1, mValues, index, mSize - (index + 1));
        mSize--;
    }

    /**
     * Adds a mapping from the specified key to the specified value,
     * replacing the previous mapping from the specified key if there
     * was one.
     */
    public final void put(final long key, final boolean value) {
        int i = ArrayHelper.binarySearch(mKeys, mSize, key);

        if (i >= 0) {
            mValues[i] = value;
        } else {
            i = ~i;

            if (mSize >= mKeys.length) {
                expandToCapacity(ArrayHelper.idealIntArraySize(mSize + 1));
            }

            if (mSize - i != 0) {
                System.arraycopy(mKeys, i, mKeys, i + 1, mSize - i);
                System.arraycopy(mValues, i, mValues, i + 1, mSize - i);
            }

            mKeys[i] = key;
            mValues[i] = value;
            mSize++;
        }
    }

    /**
     * Returns the number of key-value mappings that this SparseBooleanArray
     * currently stores.
     */
    public final int size() {
        return mSize;
    }

    /**
     * Given an index in the range <code>0...size()-1</code>, returns
     * the key from the <code>index</code>th key-value mapping that this
     * SparseBooleanArray stores.
     *
     * <p>The keys corresponding to indices in ascending order are guaranteed to
     * be in ascending order, e.g., <code>keyAt(0)</code> will return the
     * smallest key and <code>keyAt(size()-1)</code> will return the largest
     * key.</p>
     */
    public final long keyAt(final int index) {
        return mKeys[index];
    }

    /**
     * Given an index in the range <code>0...size()-1</code>, returns
     * the value from the <code>index</code>th key-value mapping that this
     * SparseBooleanArray stores.
     *
     * <p>The values corresponding to indices in ascending order are guaranteed
     * to be associated with keys in ascending order, e.g.,
     * <code>valueAt(0)</code> will return the value associated with the
     * smallest key and <code>valueAt(size()-1)</code> will return the value
     * associated with the largest key.</p>
     */
    public final boolean valueAt(final int index) {
        return mValues[index];
    }

    public final void setValueAt(final int index, final boolean value) {
        mValues[index] = value;
    }

    public final void setKeyAt(final int index, final long key) {
        mKeys[index] = key;
    }

    /**
     * Returns the index for which {@link #keyAt} would return the
     * specified key, or a negative number if the specified
     * key is not mapped.
     */
    public final int indexOfKey(final long key) {
        return ArrayHelper.binarySearch(mKeys, mSize, key);
    }

    /**
     * Returns an index for which {@link #valueAt} would return the
     * specified key, or a negative number if no keys map to the
     * specified value.
     * Beware that this is a linear search, unlike lookups by key,
     * and that multiple keys can map to the same value and this will
     * find only one of them.
     */
    public final int indexOfValue(final boolean value) {
        for (int i = 0; i < mSize; i++)
            if (mValues[i] == value) return i;

        return INVALID;
    }

    /**
     * Removes all key-value mappings from this SparseBooleanArray.
     */
    public final void clear() {
        mSize = 0;
    }

    /**
     * Puts a key/value pair into the array, optimizing for the case where
     * the key is greater than all existing keys in the array.
     */
    public final void append(final long key, final boolean value) {
        if (mSize != 0 && key <= mKeys[mSize - 1]) {
            put(key, value);
            return;
        }

        final int pos = mSize;
        if (pos >= mKeys.length) {
            expandToCapacity(ArrayHelper.idealIntArraySize(pos + 1));
        }

        mKeys[pos] = key;
        mValues[pos] = value;
        mSize = pos + 1;
    }

    /**
     * Increases the size of the underlying storage if needed, to ensure that it can
     * hold the specified number of items without having to allocate additional memory
     * @param capacity the number of items
     */
    public final void ensureCapacity(final int capacity) {
        if (mKeys.length < capacity) {
            expandToCapacity(capacity);
        }
    }

    private void expandToCapacity(final int capacity) {
        final long[] nkeys = new long[capacity];
        final boolean[] nvalues = new boolean[capacity];

        System.arraycopy(mKeys, 0, nkeys, 0, mKeys.length);
        System.arraycopy(mValues, 0, nvalues, 0, mValues.length);

        mKeys = nkeys;
        mValues = nvalues;
    }
}